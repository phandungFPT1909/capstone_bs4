$(document).ready(function () {
    $(".mobile-toggle").click(function () {
      $(this).toggleClass("active");
      if ($("#mobile-nav ul").css("display") == "none") {
        $("#mobile-nav ul").slideDown(200);
      } else {
        $("#mobile-nav ul").slideUp(200);
      }
      // $('#mobile-nav ul').toggleClass('active');
    });
  });
  
  // add class active in id="switchButton" and add class dark in id="light" ==> change light and dark
  // desktop
  const dkSwitchButton = document.querySelector("#desktop-switchButton");
  const light_A = document.querySelector("#light");
  
  dkSwitchButton.onclick = function () {
    dkSwitchButton.classList.toggle("active");
    light_A.classList.toggle("dark");
  };
  
  // mobile
  const mbSwitchButton = document.querySelector("#mobile-switchButton");
  const light_B = document.querySelector("#light");
  
  mbSwitchButton.onclick = function () {
    mbSwitchButton.classList.toggle("active");
    light_B.classList.toggle("dark");
  };
  
  
  
  
  //  add class and remove class sticky ==> create fixed navigation bar 
  window.onscroll = function () {
    myFunction()
  };
  
  var navbar = document.getElementById("navbar");
  var sticky = navbar.offsetTop;
  
  function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
  }
  